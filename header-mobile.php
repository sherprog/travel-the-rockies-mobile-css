<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );


	?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-1.5.1.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>

<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

	<link  href="http://fonts.googleapis.com/css?family=Goudy+Bookletter+1911:regular" rel="stylesheet" type="text/css" >
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/thickbox-compressed.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/thickbox.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/mobile.css" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-315024-48']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
	<div id="title">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/ttr-logo.png" width="676" height="54" />
		<div id="trail">
		</div>
		<div id="binocs">
		</div>
	</div>
	<div id="shadow">
		<div id="hero">
			<?php
				$featured_image = wyoGetFeaturedImageURL( $post->ID, 'post-thumbnail' );
				if ($featured_image == '')
					$featured_image = get_bloginfo('stylesheet_directory').'/images/hero.jpg';
			?>
			<div id="heroImage">
				<img src="<?php echo $featured_image; ?>" width="980" height="376" />
			</div>
			<div id="magnify">
				<a href="#" onclick="toggleHeroImage();"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/magnify.png" width="75" height="86" border="0" /></a>
			</div>
		</div>
		<div id="subMenu">
			<div class="subMenuBlock">
				<br />
				<ul>
					<?php
						$parent_id = wyoGetTopParentID();
						$parent_post = get_post($parent_id);
						$sub_nav_title = $parent_post->post_title;
						if (strpos($sub_nav_title,'home') !== false)
							$sub_nav_title = '';
					?>
					<b><?php echo $sub_nav_title; ?></b>
					<?php if ($parent_id != 4919) wp_list_pages("title_li=&child_of=".wyoGetTopParentID()."&exclude=342"); ?>
				</ul>

				<br />

				<img id="coach" src="<?php bloginfo('stylesheet_directory'); ?>/images/coach.png" width="110" height="76" />
				<br />
				<div id="tagline">
				We're ready! Are you?
				</div>
				<br />
			</div>
			<div class="contactBlock">
			Call Toll Free<br />
			<b><span class="phone">888-672-9466</span></b><br />
			Our Office hours<br />
			Monday - Friday<br />
			9:00 a.m. - 5:00 p.m. MST<br />
			Member of the Jackson Hole Chamber of Commerce<br />
			</div>

			<div class="contactBlock">
			<center>
			<b>Proud Members of:</b>
			<br />
			<br />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/bbbsealh1US.png" width="135" height="51" />
			<br />
			<br />
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/jackson-chamber-logo-sm.jpg" width="190" height="102" />
			<br />
			<br />
			<a href="http://www.nationalparks.org" target="_blank"><img
				src="<?php bloginfo('stylesheet_directory'); ?>/images/npf_badge-sm.png" width="135" height="136" /></a>
			<br />
			<br />
			<a href="http://www.rmef.org" target="_blank"><img
				src="<?php bloginfo('stylesheet_directory'); ?>/images/rmef-logo.jpg" width="98" height="83" /></a>
			<br />
			</center>
			</div>
		</div>
		<div id="midPage">
			<div id="mainMenu">
				<ul>
					<li id="mobile-toggle" class="page_item" ><a href="#">Menu</a></li>
					<?php wp_list_pages('title_li=&depth=1&exclude=4919'); ?>

					<?php //wp_nav_menu( array('theme_location' => 'primary-menu', 'depth' => 1) ); ?>
				</ul>
			</div>
<script type="text/javascript">

(function(a,e,c,f,g,b,d){var h={ak:"975861617",cl:"KqpKCMORpFkQ8e6p0QM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");

jQuery(document).ready(function(){
_googWcmGet('phone', '888-672-9466');
})

</script>
