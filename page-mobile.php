<?php /* Template Name: Mobile */ ?>

<?php
get_header('mobile');
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<div id="content">
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

		<div class="clearer"></div>
		<br />
	</div>

<?php endwhile; ?>

<?php
get_footer('mobile');
?>
