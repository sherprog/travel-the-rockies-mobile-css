function toggleHeroImage() {
	var imageHeight = $('#heroImage').height();
	if (imageHeight > 220) {
		$('#heroImage').animate( { height: '220px' }, { queue: false, duration: 2000 });
	}
	else {
		$('#heroImage').animate( { height: '376px' }, { queue: false, duration: 2000 });
	}
	return false;
}

$(document).ready(function() {
	if ($('#post-5620').length && $(window).width() <= 1035) {
		var mainMenu = $('#mainMenu');
		var subMenu = $('.subMenuBlock').find('ul');
		subMenu.attr('class','sub_menu');
		subMenu.insertAfter(mainMenu);
		$('#subMenu').insertAfter($('#midPage'));
	}
	var menuToggle = $('#mobile-toggle');
	var menuItems = $('#mainMenu .page_item:not(#mobile-toggle)');
	menuToggle.click(function(e) {
		e.preventDefault();
		console.log('Called');
		if ($(menuItems[0]).css('display') =='none') {
			menuItems.show();
		} else {
			menuItems.hide();
		}
		return false;
	});
});
